import React from 'react'
import { Router } from '@reach/router'

import { Logo } from './components/Logo'
import { GlobalStyles } from './styles/GlobalStyles.js'

import { Home } from './pages/Home'
import { Detail } from './pages/Detail'
import { NavBar } from './components/NavBar'
import { Favs } from './pages/Favs'
import { User } from './pages/User'
import { NotRegisterUser } from './pages/NotRegisterUser'

const UserLogged = ({ children }) => {
  return children({ isAuth: false })
}

export const App = () => {
  /*
    ! Constants not used in this project
  */
  // const urlParams = new URLSearchParams(window.location.search)
  // const detailId = urlParams.get('detail')

  return (
    <>
      <GlobalStyles />
      <Logo />

      <Router>
        <Home path='/' />
        <Home path='/pet/:id' />
        <Detail path='/detail/:id' />
      </Router>
      <UserLogged>
        {
          ({ isAuth }) =>
            isAuth
              ?
              <Router>
                <Favs path='/favs' />
                <User path='/user' />
              </Router>
              :
              <Router>
                <NotRegisterUser path='/favs' />
                <NotRegisterUser path='/user' />
              </Router>
        }
      </UserLogged>
      <NavBar />
    </>
  )
}
