import React from 'react'

import { Link, Image } from './styles.jsx'

const DEFAULT_IMAGE = 'https://i.pinimg.com/564x/be/6f/bb/be6fbb9b1419f0c200e591c1222ebc03.jpg'

export const Category = ({ cover = DEFAULT_IMAGE, path = '#', emoji = '?' }) => (
  <Link to={path}>
    <Image src={cover} alt='' />
    {emoji}
  </Link>
)
