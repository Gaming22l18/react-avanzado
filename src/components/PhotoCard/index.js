import React from 'react'
import { ImgWrapper, Img, Article } from './styles.jsx'
import { FavButton } from '../FavButton'
import { ToggleLikeMutation } from '../../containers/ToogleLikeMutation.jsx'
import { useNearScreen } from '../../hooks/useNearScreen'
import { useLocalStorage } from '../../hooks/useLocalStorage'
import { Link } from '@reach/router'

const DEFAULT_IMAGE = 'https://visionnoventa.net/wp-content/uploads/2019/11/Lauren-Summer-heylaurensummer-instagram-2.jpg'

export const PhotoCard = ({ id, likes = 0, src = DEFAULT_IMAGE }) => {
  const [show, element] = useNearScreen()
  const { mutation, mutationLoading, mutationError } = ToggleLikeMutation()
  const key = `like-${id}`
  const [liked, setLiked] = useLocalStorage(key, false)
  const handleFavClick = () => {
    !liked && mutation({
      variables: {
        input: { id }
      }
    })
    setLiked(!liked)
  }
  // console.log('{ mutation, mutationLoading, mutationError }', { mutation, mutationLoading, mutationError })

  return (
    <Article ref={element}>
      {
        show &&
          <>
            <Link to={`/detail/${id}`}>
              <ImgWrapper>
                <Img src={src} />
              </ImgWrapper>
            </Link>

            <FavButton
              liked={liked} likes={likes}
              onClick={handleFavClick}
            />
          </>
      }
    </Article>
  )
}
