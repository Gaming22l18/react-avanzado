/*
 * App component
*/
import React from 'react'

/*
 * Component
 */
import { PhotoCardWithQuery } from '../containers/PhotoCardWithQuery.jsx'

export const Detail = ({ id }) => {
  return (
    <PhotoCardWithQuery id={id} />
  )
}
