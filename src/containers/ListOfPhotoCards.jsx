import React from 'react'
import { useQuery } from '@apollo/client'

import { whitPhotos } from '../hoc/whitPhotos.js'
import { ListOfPhotoCardsComponent } from '../components/ListOfPhotoCards'

export const ListOfPhotoCards = ({ categoryId }) => {
  const { loading, error, data } = useQuery(whitPhotos, {
    variables: { categoryId }
  })
  if (error) {
    return <h2>Internal Server Error</h2>
  }
  if (loading) {
    return <h2>Loading...</h2>
  }
  return <ListOfPhotoCardsComponent data={data} />
}
