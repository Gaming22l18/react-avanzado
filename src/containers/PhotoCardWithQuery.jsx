import React from 'react'
import { gql, useQuery } from '@apollo/client'

import { PhotoCard } from '../components/PhotoCard'

const GET_SINGLE_PHOTO = gql`
  query getSinglePhoto($id: ID!) {
    photo(id: $id) {
      id
      categoryId
      src
      likes
      liked
      userId
    }
  }
`

const renderProp = (loading, error, data) => {
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error :(</p>

  const { photo = {} } = data

  return (
    <PhotoCard {...photo} />
  )
}

export const PhotoCardWithQuery = ({ id }) => {
  const { loading, error, data } = useQuery(GET_SINGLE_PHOTO, {
    variables: { id }
  })
  // eslint-disable-next-line no-lone-blocks
  return (
    <>
      {
      renderProp(loading, error, data)
    }
    </>
  )
}
